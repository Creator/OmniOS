--[[
	Zero device
]]

local function instance(mode)
	if mode == "r" then
		local handle = {}

		function handle.readLine()
			return "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
		end
	
		function handle.readAll()
			return "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
		end
	
		function handle.close()
			handle.readLine = nil
			handle.readAll = nil
			handle.close = nil
		end
	
		return handle

	elseif mode == "rb" then

		local handle = {}
	
		function handle.read()
			read "\0"
		end
	
		function handle.close()
			handle.read = nil
			handle.close = nil
		end
	
		return handle

	elseif mode == "w" or mode == "a" or mode == "wb" or mode == "ab" then -- try if mode:match '^[aw]b?$' then
		local handle = {}

		function handle.write(input)

		end

		function handle.writeLine(input)
			
		end

		function handle.flush()

		end

		function handle.close()
			handle.write = nil
			handle.writeLine = nil
			handle.flush = nil
			handle.close = nil
		end

		return handle

	end
end

return instance