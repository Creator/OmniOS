--[[
	Shell script: getfs by Creator
	for OmniOS
]]--

local path = (...) or shell.getPath() 
path = shell.resolvePath(path)

print(fs.getfs(path))