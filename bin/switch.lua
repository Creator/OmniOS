--[[
	Shell script: switch by Creator
	for OmniOS
]]--

local nID = ...

nID = tonumber(nID)
 if nID then
 	kernel.switch(nID)
 end