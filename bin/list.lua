--[[
    Shell script: list by Creator
    for OmniOS
    Edited by Piorjade: http://www.computercraft.info/forums2/index.php?/user/41415-piorjade/
]]--

local path = (...) or shell.getPath()


if fs.isDir(path) then
	for i, v in pairs(fs.list(path)) do
		if fs.isDir(path.."/"..v) then
			term.setTextColor(colors.green)
			print(v)
		else
			term.setTextColor(colors.white)
			print(v)
		end
	end
else
	term.setTextColor(colors.red)
	print("Not a directory.")
end

term.setTextColor(colors.white)