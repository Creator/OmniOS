--[[
	The OmniOS shell
	by Creator
]]--

local shell = {}
local aliases = {}
local prev = {}
local nPrev = 1
local doPrint = true
local runningChildren = 0
local runShell = true

function onDeath()
	runningChildren = runningChildren - 1
	print(" a priocess has died", runningChildren)
	if runningChildren == 0 then
		doPrint = true
	end
end

local path = "/"

local function getPath()
	return path
end

local function setPath(pth)
	if fs.exists(pth) then
		path = pth
	else
		return false, "Path does not exist"
	end
end

local function getLib(name)
	if lib[name] then
		return lib[name]
	end
end

local function stopShell()
	runShell = false
end

local function resolvePath(pth)
	if type(pth) == "string" then
		if pth:sub(1,1) == "/" then
			return fs.combine(pth, "")
		else
			return fs.combine(path, pth)
		end
	else
		return false, "<path> has to be type string."
	end
end

local shell = {
	getPath = getPath,
	setPath = setPath,
	getLib = getLib,
	stopShell = stopShell,
	resolvePath = resolvePath,
}

local function fix(t)
	local first = t[1]
	if fs.exists(path..first) and not fs.isDir(path..first) then
		t[1] = path..first
		return t
	end
	if fs.exists(path..first..".lua") and not fs.isDir(path..first..".lua") then
		t[1] = path..first..".lua"
		return t
	end
	if fs.exists("OmniOS/bin/"..first) and not fs.isDir("OmniOS/bin/"..first) then
		t[1] = "OmniOS/bin/"..first
		return t
	end
	if fs.exists("OmniOS/bin/"..first..".lua") and not fs.isDir("OmniOS/bin/"..first..".lua") then
		t[1] = "OmniOS/bin/"..first..".lua"
		return t
	end
	if aliases[first] then
		t[1] = "OmniOS/bin/"..aliases[first]..".lua"
		return t
	end
	return t, 0
end

local function subTokenize(nstr)
	local result = {}
	local current = ""
	local quotes = false
	local qtype = ""
	while nstr:sub(1,1) == " " do
		nstr = nstr:sub(2,-1)
	end
	for i=1, #nstr do
		local char = nstr:sub(i,i)
		if char == "\"" or char == "'" then
			if quotes then
				if qtype == char then
					quotes = false
					result[#result+1] = current
					current = ""
					qtype = ""
				else
					current = current..char
				end
			else
				quotes = true
				qtype = char
				if not (current == "") then
					result[#result+1] = current
					current = ""
				end
			end
		elseif char == " " then
			if quotes then
				current = current..char
			else
				if not (current == "") then
					result[#result+1] = current
					current = ""
				end
			end
		else
			current = current..char
		end
		if i == #nstr and current ~= "" then
			result[#result+1] = current
			current = ""
		end
	end
	return result
end

local function tokenize(str)
	local parts = {}
	for token in str:gmatch("[^|]+") do
		parts[#parts+1] = subTokenize(token)
	end
	return parts
end

local function getFileContent(pth)
	local file = fs.open(pth,"r")
	local data = file.readAll()
	file.close()
	return loadstring(data, pth)
end

local function run(input)
	local parsed = tokenize(input)
	local code = nil
	local which
	for i=1, #parsed do
		parsed[i], code = fix(parsed[i])
		if code == 0 then
			which = which or i
		end
	end
	if code then
		print(parsed[which][1].." does not exist.")
		return false
	end
	local ids = {}
	if parsed[2] then
		doPrint = false
		log.stdout("Found several programs with piping.")
		local pipes = {}
		local handles = {}
		for i=1, #parsed do
			if i > 1 then
				local id, handle = kernel.newProc(getFileContent(parsed[i][1]), term.current(), 1, parsed[i][1], {["shell"] = shell, ["log"] = log}, unpack(parsed[i], 2))
				pipes[i-1] = kernel.newPipe(id)
				handles[i-1].pipe(pipes[i-1].deathHandle)
				handles[i] = handle
			else
				local id, handle = kernel.newProc(getFileContent(parsed[i][1]), term.current(), 1, parsed[i][1], {["shell"] = shell, ["log"] = log}, unpack(parsed[i], 2))
				handles[i] = handle
			end
		end
		for i=1, #parsed do	
			log.stdout("Initialized", parsed[i][1])
			handles[i].onDeath(onDeath)
			if i == 1 then
				handles[i].stdout(pipes[1].write)
				handles[i].sendEvents(true)
				handles[i].getOutput(true)
			elseif i == #parsed then
				handles[i].stdin(pipes[i-1].read)
				handles[i].getOutput(true)
			else
				handles[i].stdout(pipes[1].write)
				handles[i].stdin(pipes[i-1].read)
				handles[i].getOutput(true)
			end 
		end
		runningChildren = #parsed
		log.stdclose()
	else
		--dofile(parsed[1][1], unpack(parsed[1], 2))
		local loadedfile, err = getFileContent(parsed[1][1])
		if not loadedfile then print(err) end
		local env = sandbox({["shell"] = shell, ["log"] = log, ["kernel"] = kernel})
		setfenv(loadedfile, env)
		local ok, err = pcall(loadedfile,unpack(parsed[1], 2))
		if not ok then print(err) end
	end
end

local function isEmpty(str)
	local yes = true
	for i=1, #str do
		yes = str:sub(i,i) == " " and yes 
	end
	return yes
end

term.setTextColor(colors.yellow)
print("OmniOS Shell v1.0")
while runShell do
	if doPrint then
		term.setTextColor(colors.yellow)
		term.write((path == "/" and "" or path)..	"> ")
		term.setTextColor(colors.white)
	end
	local input = read(nil, prev)
	if not isEmpty(input) then
		prev[nPrev] = input
		nPrev = nPrev + 1
		run(input)
	end
end