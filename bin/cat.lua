--[[
	Shell script: cat by Creator
	for OmniOS
	edited by Piorjade: http://www.computercraft.info/forums2/index.php?/user/41415-piorjade/
]]--

--[[local path = ...

if fs.exists(path) then
	local file = fs.open(path,"r")
	local data = file.readAll()
	file.close()
	print(data)
else
	print("File does not exist.")
end]]--

local path = (...) or shell.getPath()

if fs.exists(path) and not fs.isDir(path) then
	local file = fs.open(path,"r")
	local data = file.readAll()
	file.close()
	print(data)
elseif fs.isDir(path) then
	term.setTextColor(colors.red)
	print("Path is a folder.")
else
	print("File does not exist.")
end