--[[
	Shell script: crc by Creator
	for OmniOS
]]--

local function isEmpty(str)
	local yes = true
	for i=1, #str do
		yes = str:sub(i,i) == " " and yes 
	end
	return yes
end

local data = read()
local crc = shell.getLib("crc")

while data do
	if isEmpty(data) then
		break
	end
	print(crc(data))
	data = read()
end