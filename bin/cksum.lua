--[[
	Shell script: cksum by Creator
	for OmniOS
]]--

local path = ...

if path and fs.exists(path) and not fs.isDir(path) then
	local file = fs.open(path,"r")
	local data = file.readAll()
	file.close()
	local crc = shell.getLib("crc")
	print(crc(data))
else
	term.setTextColor(colors.red)
	print("Not a file.")
end

term.setTextColor(colors.white)