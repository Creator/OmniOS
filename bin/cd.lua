--[[
	Shell script: cd by Creator
	for OmniOS
]]--

local nPath = ...

if nPath == ".." then
	shell.setPath(fs.combine(shell.getPath(), ".."))
else
	if nPath:sub(1,1) == "/" then
		nPath = fs.combine(nPath, "")
		shell.setPath(nPath)
	else
		nPath = fs.combine(nPath, "")
		print(fs.combine(shell.getPath(), nPath))
		shell.setPath(fs.combine(shell.getPath(), nPath))
	end
end