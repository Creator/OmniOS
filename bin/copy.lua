--[[
	Shell script: cp by Creator
	for OmniOS
]]--

local path1, path2 = ...

if path1 and path2 then
	if fs.exists(path1) then
		if fs.exists(path2) then
			term.setTextColor(colors.red)
			print("The second path already exists.")
		else
			fs.copy(path1, path2)
		end
	else
		term.setTextColor(colors.red)
		print("One of the two paths is not valid.")
	end
else
	term.setTextColor(colors.red)
	print("Provide 2 arguments.")
end

term.setTextColor(colors.white)