--[[
	Shell script: kill by Creator
	for OmniOS
]]--

local id = ...

id = tonumber(id)

if id then
	kernel.kill(id)
	print("Killed process", id)
end