--[[
	Shell script: tac by Creator
	for OmniOS
]]--

local function isEmpty(str)
	local yes = true
	for i=1, #str do
		yes = str:sub(i,i) == " " and yes 
	end
	return yes
end

local path = ...

local data = read()

local file = fs.open(path,"a")

while data do
	if isEmpty(data) then
		break
	end
	file.write(data.."\n")
	file.flush()
	data = read()
end

file.close()