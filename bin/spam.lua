--[[
	Shell script: spam by Creator
	for OmniOS
]]--

local amount, name, path = ...
amount = tonumber(amount)

if not amount or amount <= 0 then
	return false
end

if not (name or path) or not fs.exists(path) then
	print("Some error occured.")
	return
end

local file = fs.open(path, "r")
local data = file.readAll()
file.close()

local func, err = loadstring(data, name)

if func then
	--kernel.newProc(func, parent, level, name, ...)
	for i=1, amount do
		kernel.newProc(func, term.current(), 0, name.."_"..tostring(i), ...)
	end
else
	print(err)
end