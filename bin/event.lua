--[[
	Shell script: event by Creator
	for OmniOS
]]--

while true do
	local event = {os.pullEvent()}
	print(unpack(event))
	if event[1] == "terminate" then
		break
	end
end