--[[
	Shell script: np by Creator
	for OmniOS
]]--

local name, path = ...

if not (name or path) or not fs.exists(path) or fs.isDir(path) then
	print("Some error occured.")
	return
end

local file = fs.open(path, "r")
local data = file.readAll()
file.close()

local func, err = loadstring(data, name)

local function stripTwo(a, b, ...)
	return ...
end

if func then
	print(kernel.newProc(func, "term", 1, name, nil, stripTwo(...)))
else
	print(err)
end