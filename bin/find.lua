--[[
	Shell script: find by Creator
	for OmniOS
]]--

local search = ...
local path = shell.getPath()

local res = fs.find(fs.combine(path, search))

for i,v in pairs(res) do
	if fs.isDir(path.."/"..v) then
		term.setTextColor(colors.green)
		print(v)
	else
		term.setTextColor(colors.white)
		print(v)
	end
end

term.setTextColor(colors.white)