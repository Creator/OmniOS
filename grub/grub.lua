--[[
	OS selector by Creator
]]--

local oses = {}
local paths = {}
local selected = 1
local amount
local timer
local timeLeft = 5
local fromEnter = false

local function loadoses()
	local file = fs.open("OmniOS/etc/grub/oslist.cfg","r")
	local data = file.readAll()
	file.close()
	for token in data:gmatch("[^\n]+") do
		local o, path = token:match("([^:]+):([^:]+)")
		oses[selected] = o
		paths[selected] = path
		selected = selected + 1
	end
	amount = selected - 1
	selected = 1
end

local function draw()
	term.clear()
	term.setCursorPos(2,1)
	term.write("Please select the OS you want to boot.")
	for i, v in pairs(oses) do
		term.setTextColor(colors.blue)
		term.setCursorPos(2,i+1)
		term.write("[ ] "..v)
	end
	term.setCursorPos(2, #oses+2)
	term.setTextColor(colors.lightGray)
	term.write("For settings, press <S>")
	term.setCursorPos(2, #oses+3)
	term.write("Time left: "..tostring(timeLeft))
end

local function redraw()
	term.setTextColor(colors.orange)
	for i, v in pairs(oses) do
		term.setCursorPos(3,i+1)
		if i == selected then
			term.write("x")
		else
			term.write(" ")
		end
	end
	term.setCursorPos(2, #oses+3)
	term.write("Time left: "..tostring(timeLeft))
end

loadoses()
draw()
timer = os.startTimer(1)

while true do
	
	local e = {os.pullEvent()}
	if e[1] == "key" then
		if e[2] == keys.up then
			selected = selected - 1
			if selected == 0 then
				selected = amount
			end
		elseif e[2] == keys.down then
			selected = selected + 1
			if selected == amount + 1 then
				selected = 1
			end
		elseif e[2] == keys.enter then
			fromEnter = true
			break
		end
	elseif e[1] == "timer" and e[2] == timer then
		timeLeft = timeLeft - 1
		if timeLeft == 0 then
			break
		else
			timer = os.startTimer(1)
		end
	end
	redraw()
end

local function dofile(path,...)
	local func, err = loadfile(path)
	if func then
		setfenv(func,_G)
		return pcall(func, ...)
	else
		return false, err
	end
end

if fromEnter then
	--os.run(_G,paths[selected])
	print(paths[selected])
	ok, err = dofile(paths[selected])
	local file = fs.open("logs/boot.log","a")
	file.write(tostring(ok)..err.."\n")
	file.close()
else
	os.run(_G,paths[1])
end