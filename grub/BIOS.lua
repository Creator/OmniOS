--[[
	BIOS by Creator
	for OmniOS
]]--

term.redirect(term.native())

--Variables--
local gui = {}
local OS = {}
local timeLeft = 5
local currentOS = 1
local defaultOS = ""
local toBoot = 0
local layout = [[
     +-----------------------------------------+
     | Current selection:                      |
     |                                         |
     +-----------------------------------------+
     | Available OSes:                         |
     |                                         |
     |                                         |
     |                                         |
     |                                         |
     |                                         |
     |                                         |
     +-----------------------------------------+
     | S: Settings                             |
     +-----------------------------------------+
     | Time left:                              |
     +-----------------------------------------+
]]
--Functions
function gui.clear()
	term.setBackgroundColor(colors.blue)
	term.setTextColor(colors.white)
	term.setCursorPos(1,2)
	term.clear()
end

function gui.drawMain()
	gui.clear()
	term.setCursorPos(1,3)
	print(layout)
	term.setCursorPos(8,4)
	term.write(OS[currentOS][1])
	for i = 1, #OS do
		term.setCursorPos(8,i+6)
		term.write(i..") "..OS[i][1])
	end
	term.setCursorPos(19,16)
	term.write(timeLeft)
end

local function loadOS()
	return dofile("OmniOS/BIOS/List")
end

local function loadDefault()
	return dofile("OmniOS/BIOS/default")
end

local function findCurrent()
	for i, v in pairs(OS) do
		if defaultOS == v[1] then
			return i
		end
	end
	error("The OS you are searching does not exist!")
end

local function settings()
  while true do
    local char, _
    gui.clear()
    print("To add an OS, press A, to quit, press Q.")
    repeat
      _, char = coroutine.yield("char")
    until char == "a" or char == "q"
    if char == "a" then
      print("Please write the name you wish to be displayed:")
      local name = read()
      print("Now write the path to the file that should be run first:")
      local path = read()
      print("If you want to confirm, write \"YES\".")
      local confirm = read()
      if confirm == "YES" then
        local list = dofile("OmniOS/BIOS/List")
        local free = 0
        while true do
          free = free + 1
          if not list[free] then
            break
          end
        end
        list[free] = {name,path}
        local file = fs.open("OmniOS/BIOS/List","w")
        file.write("return "..textutils.serialize(list))
        file.close()
        list = nil
        free = nil
      else
        print("You cancelled the process!")
      end
    elseif char == "r" then

    elseif char == "q" then
      break
    end
  end
end

--Code
OS = loadOS()
defaultOS = loadDefault()
currentOS = findCurrent()
timerID = os.startTimer(1)
while true do
	gui.drawMain()
	local event = {os.pullEvent()}
	if timeLeft == 0 then
		toBoot = currentOS
		break
	end
	if event[1] == "key" then
		--os.cancelTimer(timerID)
		if 2 <= event[2] and event[2] <= 11 then
			event[2] = event[2] == 11 and 0 or event[2] - 1
			if OS[event[2]] then
				toBoot = event[2]
				break
			end
		elseif event[2] == keys.up then
			currentOS = currentOS - 1
			currentOS = currentOS == 0 and #OS or currentOS
		elseif event[2] == keys.down then
			currentOS = currentOS + 1
			currentOS = currentOS == #OS + 1 and 1 or currentOS
		elseif event[2] == keys.enter then
			toBoot = currentOS
			break
    elseif event[2] == 31 then
      settings()
      timeLeft = 0
      timerID = os.startTimer(1)
		end
	elseif event[1] == "timer" and event[2] == timerID then
		timeLeft = timeLeft - 1
    timerID = os.startTimer(1)
	end
end

if OS[toBoot][1] == "CraftOS" then
	term.setBackgroundColor(colors.black)
	term.setTextColor(colors.yellow)
	term.clear()
	term.setCursorPos(1,1)
	term.write("CraftOS 1.7")
else
	dofile(OS[toBoot][2])
end
