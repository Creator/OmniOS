--[[
	Kernel
	By: Creator a.k.a. Wassil Janssen
	For OmniOS
]]--
--[[
	Specifications:
	newProcess:
		@parent: the object on which the process will be drawn
		@func: the function, usually from a file
]]--
os.pullEvent = os.pullEventRaw
--Variables
local ids = {}
local process = {}
local pids = {}
local tasks = {}
local active = 1
local bindings = {}
local eventFilter = {["key"] = true, ["mouse_click"] = true, ["paste"] = true,["char"] = true, ["terminate"] = true, ["mouse_scroll"] = true, ["mouse_drag"] = true, ["key_up"] = true, ["mouse_up"] = true}
local Internal = {}
local eventBuffer = {}
local nameIDLink = {}
local currTerm = term.current()
local w,h = term.getSize()
local running = 1
local skipEvent = false

Kernel = {}
Internal.messages = {}

--Overwrites
function assert(check,msg,level)
	if not type(level) == "number" then error("The third argument has to be a number!",2) end
	if not check then error(msg,level+1) end
end

--Functions
--Local
function Internal.finalKill(id)
	if tonumber(id) and process[id] then
		process[id] = nil
	end
end

function Internal.kill(id)
	if id == 1 then
		return
	elseif id == active then
		Kernel.switch(1)
	end
	Internal.queueTask("finalKill",id)
end

function Internal.drawClosed()
	local col = process[active].preferences.sidebarColor or colors.black
	paintutils.drawLine(w,1,w,h,col)
	term.setBackgroundColor(col)
	term.setCursorPos(w,math.floor(h/2)+1)
	term.setTextColor(process[active].preferences.sidebarTextColor or colors.white)
	term.write("<")
end

function Internal.drawOpen()
	process[active].window.setVisible(false)
	term.setCursorBlink(false)
	paintutils.drawFilledBox(w-15,1,w,h,colors.black)
	term.setTextColor(colors.white)
	local amount = 1
	local links = {}
	for i,v in pairs(process) do
		if active == i then paintutils.drawLine(w-15,amount,w,amount,colors.blue) end
		term.setCursorPos(w-14,amount)
		term.setBackgroundColor(active == i and colors.blue or colors.black)
		term.write("x "..i.." "..v.name)
		links[amount] = i
		amount = amount + 1
	end

	while true do
		local evnt = {os.pullEventRaw()}
		if evnt[1] == "mouse_click" then
			if evnt[3] < w-15 then break
			elseif evnt[3] == w-14 and evnt[4] <= amount then
				Kernel.kill(links[evnt[4]])
				--if links[evnt[4]] == active then break end
				break
			elseif evnt[3] >= w-12 and evnt[4] <= amount then
				Kernel.switch(links[evnt[4]])
				break
			end
		elseif not (evnt[1] == "mouse_up" or evnt[1] == "mouse_drag") then
			eventBuffer[#eventBuffer+1] = evnt
		end
	end
	process[active].window.setVisible(true)
	process[active].window.redraw()
	Internal.drawClosed()
end


function Internal.newProcess(func,parent,permission,name,id,...)
	id = id or 1
	--Utils.debug("ID is "..tostring(id))
	if permission == "user" then
		local env = Sandbox.newEnv(name)
		setfenv(func,env)
	end
	process[id] = {
		--["environement"] = env,
		["routine"] = coroutine.create(func or (function()print("erroe")end)),
		["name"] = name,
		["filter"] = "",
		["parent"] = parent,
		["window"] = window.create(parent == "term" and term.current() or peripheral.wrap(parent),1,1,w-1,h,false),
		["preferences"] = {}
	}

	if not nameIDLink[name] then nameIDLink[name] = {id} else nameIDLink[name][#nameIDLink[name]+1] = id end
	--Utils.debug("Added links.")
	running = id
	if id == active then
		process[id].window.setVisible(true)
	end
	term.redirect(process[id].window)
	ok, process[id].filter = coroutine.resume(process[id].routine,...)
	if not ok then
		print(process[id].filter)
		Utils.log("Error",tostring(ok).." && "..tostring(process[id].filter),process[id].name)
	elseif coroutine.status(process[id].routine) == "dead" then
		Kernel.kill(id)
		print(process[id].filter)
	end
	Utils.log("Filter",tostring(ok).." && "..tostring(process[id].filter),process[id].name)
	term.redirect(currTerm)
	return id
end

function Internal.runBinds(event)
	if bindings[event[1]] then
		for i,v in pairs(bindings[event[1]]) do
			if type(v) == "function" then
				pcall(v,unpack(event))
			else
				coroutine.resume(v,unpack(event))
			end
		end
	end
end

function Internal.queueTask(...)
	tasks[#tasks+1] = {...}
end

function Internal.switch(...)
  local args={...}
  Utils.debug("Switch in kernel "..(function(tabl) local x = "" for i,v in pairs(tabl) do x = x.." "..tostring(i)..": "..tostring(v) end return x end)(args))
  if not type(args[1]) == "number" then Utils.debug("Expected number, got "..type(args[1])) return end
	Utils.debug("Args "..tostring(args[1])..type(args[1]))
	if process[args[1]] then
		process[active].window.setVisible(false)
		process[args[1]].window.setVisible(true)
  	process[args[1]].window.redraw()
		active = args[1]
	end
end

function Internal.skipEvent(bool)
	skipEvent = type(bool) == "boolean" and bool or false
end

--API
function Kernel.newProcess(func,parent,permission,name,...)
	local id = 1
	while true do
		if not process[id] then break end
		id = id + 1
	end
	Utils.debug("ID is t "..tostring(id))
	process[id] = {}
	Internal.queueTask("newProcess",func,parent,permission,name,id,...)
	Utils.debug("New ID: "..tostring(id))
	return id
end

function Kernel.list()
	local ret = {}
	for i,v in pairs(process) do
		ret[#ret+1] = {i,v.name,coroutine.status(v.routine)}
	end
	return ret
end

function Kernel.switch(newTaskID)
	Utils.debug("Queueing switch event: "..tostring(newTaskID))
	Internal.queueTask("switch",newTaskID)
end

function Kernel.bindEvent(event,func)
	if not (type(func) == "function" or type(func) == "thread") then error("Ecxpected function, got "..type(func).."!",2) end
	if bindings[event] then
		bindings[event][#bindings[event]+1] = func
	else
		bindings[event] = {[1] = func}
	end
end

function Kernel.kill(id)
	Utils.debug("Killing "..tostring(id))
	Internal.queueTask("kill",id)
end

function Kernel.getRunning()
	return running
end

function Kernel.addMessage(destination,sender,message)
	if not Internal.messages[destination] then
		Internal.messages[destination] = {}
	end
	Internal.messages[destination][#Internal.messages[destination]+1] = {sender,message}
end

function Kernel.setSidebarColor(id,color)
	if not process[id] then return end
	process[id].preferences.sidebarColor = color
end

function Kernel.setSidebarTextColor(id,color)
	if not process[id] then return end
	process[id].preferences.sidebarTextColor = color
end

function Kernel.getName(id)
	return process[id] and process[id].name or "unknown"
end

function Kernel.broadcast(sender,message)
	for i,v in pairs(process) do
		if not Internal.messages[i] then
			Internal.messages[i] = {}
		end
		Internal.messages[i][#Internal.messages[i]+1] = {sender,message}
	end
end

function Kernel.getMessages(id)
	local buffer = Internal.messages[id]
	Internal.messages[id] = nil
	return buffer
end

--Code
dofile("OmniOS/Drivers/FS.lua")
dofile("OmniOS/Drivers/disk.lua")

local function keySwitch(_event,_code)
	while true do
  	if _event == "key" and _code == 56 then
    	time = os.clock()
	  elseif _event == "char" and tonumber(_code) then
    	if os.clock()-time <= 0.50 then
      	Kernel.switch(tonumber(_code))
    	end
	  end
		_event, _code = coroutine.yield()
	end
end

local keySwitchCoro = coroutine.create(keySwitch)

Kernel.bindEvent("key",keySwitchCoro)
Kernel.bindEvent("char",keySwitchCoro)

Internal.newProcess(...)

fs.link("Programs","OmniOS/Programs")
fs.link("OmniOS/usr/Programs","OmniOS/Programs")
Internal.drawClosed()

while true do
	if tasks[1] then
		if Internal[tasks[1][1]] then
			Internal[tasks[1][1]](unpack(tasks[1],2))
			Utils.debug("Completed task "..tasks[1][1])
			table.remove(tasks,1)
		end
	else
		local event = #eventBuffer == 0 and {os.pullEvent()} or table.remove(eventBuffer,1)
		Internal.runBinds(event)
		if event[1] == "mouse_click" and event[3] == w then
			Internal.drawOpen()
		elseif event[1] == "terminate" then
			Utils.log("Terminate",active,Kernel.getName(active))
			Kernel.kill(active)
		elseif not skipEvent then
			if eventFilter[event[1]] then
				if process[active].filter == nil or process[active].filter == "" or process[active].filter == event[1] then
					running = active
					Internal.drawClosed()
					Utils.debug("Running main routine with event "..tostring(event[1]))
					term.redirect(process[active].window)
					process[active].window.setVisible(true)
					ok, process[active].filter = coroutine.resume(process[active].routine,unpack(event))
					if not ok then
						print(process[active].filter)
						Utils.log("Error",tostring(ok).." && "..tostring(process[active].filter),process[active].name)
					elseif coroutine.status(process[active].routine) == "dead" then
						Kernel.kill(active)
						print(process[active].filter)
					end
					Utils.log("Filter",tostring(ok).." && "..tostring(process[active].filter),process[active].name)
					term.redirect(currTerm)
				end
			else
				for i,v in pairs(process) do
					if process[i].filter == nil or process[i].filter == "" or process[i].filter == event[1] then
						Utils.debug("Running secondary routine with event "..tostring(event[1]))
						term.redirect(v.window)
						running = i
						ok, process[i].filter = coroutine.resume(v.routine,unpack(event))
						if coroutine.status(process[i].routine) == "dead" or not ok then
							Utils.log("Error",tostring(ok).." && "..tostring(process[i].filter),process[i].name) --Kernel.kill(i)
							--term.clear()
							--term.setCursorPos(1,1)
							print(process[i].filter)
						end
						Utils.log("Filter",tostring(ok).." && "..tostring(process[i].filter),process[i].name)
						term.redirect(currTerm)
					end
				end
			end
			local buffer = {}
			for i,v in pairs(process) do
				if v.filter == "kernel_messages" and Internal.messages[i] then
					running = i
					term.redirect(process[i].window)
					ok, process[i].filter = coroutine.resume(process[i].routine,unpack(Internal.messages[i]))
					term.redirect(currTerm)
					Utils.log("MessageDelivered",Kernel.getName(i))
					Utils.log("Filter",tostring(ok).." && "..tostring(process[i].filter),process[i].name)
					if coroutine.status(process[i].routine) == "dead" or not ok then
						Utils.log("Error",tostring(ok).." && "..tostring(process[i].filter),process[i].name) --Kernel.kill(i)
						--term.clear()
						--term.setCursorPos(1,1)
						print(process[i].filter)
					end
					Internal.messages[i] = nil
				end
			end
		else
			skipEvent = false
		end
	end
end
