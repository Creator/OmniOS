--[[
	Driver loading mechanism for OmniOS
]]--

local pts, dofile, lib = ...

for i, v in pairs(fs.list("OmniOS/kernel/drivers")) do
	if fs.isDir("OmniOS/kernel/drivers/"..v) then
		pts("Loading \027+d@"..v.." \027+b@driver into memory...")
		dofile("OmniOS/kernel/drivers/"..v.."/"..v..".lua", lib, pts, dofile)
		pts("OmniOS/kernel/drivers/"..v.."/"..v..".lua")
	end
end

pts("Done")