--[[
	FS overwrite
	Author: Wassil Janssen a.k.a. Creator
]]--

local lib, pts, dofile = ...

local altFS = {}

for i,v in pairs(fs.list("OmniOS/kernel/drivers/fs")) do
	if fs.isDir("OmniOS/kernel/drivers/fs/"..v) then
		altFS[v:match("[^%.]+")] = dofile("OmniOS/kernel/drivers/fs/"..v.."/"..v..".lua", lib, altFS)
		pts("Loading fs: \027+d@"..v:match("[^%.]+"))
	end
end

pts("Loading fsh...")
local fsh = dofile("OmniOS/kernel/drivers/fs/fsh.lua", altFS)
--Functions

fs.getfs = fsh.getfs
fs.getMountPath = fsh.resolveLinks
fs.setfs = fsh.setfs
fs.link = fsh.link
fs.fs = lib.utils.table.copy(altFS)

function fs.isReadOnly(path)
	local path = fsh.resolveLinks(path)
	local fss = fs.getfs(path)
	return altFS[fss].isReadOnly(path)
end

function fs.list(path)
	local path = fsh.resolveLinks(path)
	local fss = fs.getfs(path)
	return altFS[fss].list(path)
end

function fs.exists(path)
	local path = fsh.resolveLinks(path)
	local fss = fs.getfs(path)
	return altFS[fss].exists(path)
end

function fs.isDir(path)
	local path = fsh.resolveLinks(path)
	local fss = fs.getfs(path)
	--lib.log.log("fs", path, fss)
	--ffs = altFS[fss] or altFS.ccfs
	return altFS[fss].isDir(path)
end

function fs.getDrive(path)
	local path = fsh.resolveLinks(path)
	local fss = fs.getfs(path)
	--local ffs = altFS[fss] or altFS.ccfs
	return altFS[fss].getDrive(path)
end

function fs.getSize(path)
	local path = fsh.resolveLinks(path)
	local fss = fs.getfs(path)
	--local ffs = altFS[fss] or altFS.ccfs
	return altFS[fss].getSize(path)
end

function fs.makeDir(path)
	local path = fsh.resolveLinks(path)
	local fss = fs.getfs(path)
	return altFS[fss].makeDir(path)
end
--[[Elaborate]]--
function fs.copy(path1,path2)
	if fs.isDir(path1) then
		local function explore(dir)
			local buffer = {}
			for i,v in pairs(fs.list(dir)) do
				if fs.isDir(dir.."/"..v) then
					buffer[v] = explore(dir.."/"..v)
				else
					buffer[v] = readFile(dir.."/"..v)
				end
			end
			return buffer
		end
		local function writeFile(path,content)
			local file = fs.open(path,"w")
			file.write(content)
			file.close()
		end
		local function writeDown(input,dir)
				for i,v in pairs(input) do
				if type(v) == "table" then
					writeDown(v,dir.."/"..i)
				elseif type(v) == "string" then
					writeFile(dir.."/"..i,v)
				end
			end
		end
		writeDown(explore(path1),path2)
	else
		local f = fs.open(path1,"r")
		local m = fs.open(path2,"w")
		m.write(f.readAll())
		f.close()
		m.close()
	end
end

function fs.move(path1,path2)
	if fs.isDir(path1) then
		local function explore(dir)
			local buffer = {}
			for i,v in pairs(fs.list(dir)) do
				if fs.isDir(dir.."/"..v) then
					buffer[v] = explore(dir.."/"..v)
				else
					buffer[v] = readFile(dir.."/"..v)
				end
			end
			return buffer
		end
		local function writeFile(path,content)
			local file = fs.open(path,"w")
			file.write(content)
			file.close()
		end
		local function writeDown(input,dir)
				for i,v in pairs(input) do
				if type(v) == "table" then
					writeDown(v,dir.."/"..i)
				elseif type(v) == "string" then
					writeFile(dir.."/"..i,v)
				end
			end
		end
		writeDown(explore(path1),path2)
	else
		local f = fs.open(path1,"r")
		local m = fs.open(path2,"w")
		m.write(f.readAll())
		f.close()
		m.close()
	end
	fs.delete(path1)
end

function fs.delete(path)
	local path = fsh.resolveLinks(path)
	local fss = fs.getfs(path)
	return altFS[fss].delete(path)
end

function fs.open(path,mode)
	local path = fsh.resolveLinks(path)
	local fss = fs.getfs(path)
	return altFS[fss].open(path,mode)
end

fs.setfs("OmniOS/dev","devfs")
fs.setfs("ramfs","ramfs")