--[[
	Everything is a file!
	Author: Wassil Janssen a.k.a. Creator
]]--
--[[
local lib = ...

--Variables
devfs = {}

local oldFS = lib.utils.table.copy(fs)
local peripheral = lib.utils.table.copy(peripheral)
devfs.getName = oldFS.getName
devfs.getSize = oldFS.getSize
devfs.isDir = oldFS.isDir
devfs.getDrive = oldFS.getDrive

--Functions
function devfs.isReadOnly() return false end
function devfs.getFreeSpace() return oldFS.getFreeSpace('OmniOS') end
function devfs.makeDir() return false end
function devfs.move() return false end
function devfs.copy() return false end
function devfs.delete() return false end

function devfs.list()
	local ret = {}
	for i,v in pairs(peripheral.getNames()) do
		ret[i] = peripheral.getType(v).."_"..v
	end
	return ret
end

local function wow(path)
	for i,v in pairs(devfs.list()) do 
		if v == path then 
			return true 
		end 
	end
	return fs.fs.ccfs.exists(path)
end

function devfs.exists(path)
	local path = oldFS.getName(path)
	print(path, "dev")
	return peripheral.isPresent(path) or wow(path)
end

function devfs.open(path)
	assert(path:find("_"),"Path is invalid")
	assert(peripheral.isPresent(path:sub(path:find("_")+1,-1)),"No such peripheral!")
	local side = path:sub(path:find("_")+1,-1)
	local handle = {
		ioctl = peripheral.wrap(side),
		close = function() handle = nil end,
	}
	local handle_mt = {
		__call = function(method,...)
			return peripheral.call(side,
			method,...)
		end,
		__metatable = function() return {} end,
	}
	setmetatable(handle.ioctl,handle_mt)
	setmetatable(handle,{__index = handle.ioctl})
	return handle
end

function devfs.setfs()

end

return devfs
]]

--[[
	TMPFS driver developed by Wassil Janssen a.k.a. Creator
]]--

lib = ...

devfs = {}
local Internal = {}
filesystem = {}

function Internal.isDir(path,tabl)
	path = fs.combine("",path)
	local first = Internal.getFirstElement(path)
	if first == path and tabl[first] then
		if type(tabl[first]) == "table" then
			return true
		elseif type(tabl[first]) == "string" then
			return false
		end
	elseif tabl[first] then
		if type(tabl[first]) == "table" then
			return Internal.isDir(string.sub(path,#first + 2,-1),tabl[first])
		elseif type(tabl[first]) == "string" then
			return false
		end
	end
	return false
end

function Internal.getFirstElement(path)
	if not type(path) == "string" then error("This is not a string: "..tostring(path),2) end
	return string.sub(path,1,string.find(path,"/") and string.find(path,"/")-1 or -1)
end

function Internal.exists(path,tabl)
	path = fs.combine("",path)
	local first = Internal.getFirstElement(path)
	if tabl[first] then
		if type(tabl[first]) == "table" then
			if first == path then return true end
			return Internal.exists(string.sub(path,#first + 2,-1),tabl[first])
		elseif first == path then
			return true
		end
	end
	return false
end

function Internal.read(path,tabl)
	path = fs.combine("",path)
	local first = Internal.getFirstElement(path)
	if tabl[first] then
		if type(tabl[first]) == "table" then
			if first == path then return false end
			return Internal.read(string.sub(path,#first + 2,-1),tabl[first])
		else
			if first == path then
				return tabl[first]
			else
				return false
			end
		end
	end
	return false
end

function Internal.write(data, path, tabl)
	--lib.log.log("devfs", "Writing data", data, path, tabl)
	path = fs.combine("", path)
	local first = Internal.getFirstElement(path)
	if type(tabl[first]) == "table" then
		if first == path then return false end
		return Internal.write(data, string.sub(path,#first + 2,-1), tabl[first])
	else
		--lib.log.log("devfs", "Checking", first, path)
		if first == path then
			tabl[first] = data
		else
			return false
		end
	end
	return false
end

function Internal.delete(path,tabl)
	path = fs.combine("",path)
	local first = Internal.getFirstElement(path)
	if tabl[first] then
		if type(tabl[first]) == "table" then
			if first == path then
				tabl[first] = nil
				return true
			end
			return Internal.delete(string.sub(path,#first + 2,-1),tabl[first])
		elseif type(tabl[first]) == "string" then
			if first == path then
				tabl[first] = nil
			else
				return false
			end
		end
	end
	return false
end

function Internal.list(path,tabl)
	lib.log.log("devfs", path)
	path = fs.combine("", path)
	local first = Internal.getFirstElement(path)
	if tabl[first] then
		if type(tabl[first]) == "table" then
			if first == path then
				return tabl[first]
			end
			return Internal.list(string.sub(path,#first + 2,-1),tabl[first])
		else
			return false
		end
	end
	return false
end

function Internal.makeDir(path,tabl)
	if path == nil or path == "" then
		return
	end
	lib.log.log("devfs", "makeDir: Making folder at ", path)
	path = fs.combine("",path)
	local first = Internal.getFirstElement(path)
	lib.log.log("devfs", "makeDir: First is ", first)
	if tabl[first] then
		lib.log.log("devfs", "makeDir: ", path, "exists", tabl[first])
		if type(tabl[first]) == "table" then
			if first == path then
				return true
			end
		elseif type(tabl[first]) == "string" then
			return false
		else
			return false
		end
	else
		tabl[first] = {}
		if first == path then return end
	end
	return Internal.makeDir(string.sub(path,#first + 2,-1),tabl[first])
end

--End of internal stuff!

function devfs.open(path,mode)
	if Internal.exists(path, filesystem) and not Internal.isDir(path, filesystem) then
		local x = Internal.read(path, filesystem)
		if type(x) == "function" then
			return x(mode)
		else
			return false
		end
	else
		return nil
	end
end

function devfs.isReadOnly()
	return false
end

function devfs.delete(path)
	return Internal.delete(path,filesystem)
end

function devfs.isDir(path)
	return Internal.isDir(path,filesystem)
end

function devfs.getDrive(path)
	fs.fs.ccfs.getDrive(Internal.getFirstElement(path))
end

function devfs.getSize(path)
	if devfs.exists(path) and not devfs.isDir(path) then
		return #Internal.read(path)
	else
		return 0
	end
end

function devfs.exists(path)
	Internal.exists(path,filesystem)
end

function devfs.list(path)
	if devfs.isDir(path) then
		local final = {}
		for i,v in pairs(Internal.list(path,filesystem)) do
			final[#final+1] = i
		end
		return final
	else
		return false
	end
end

function devfs.makeDir(path)
	return Internal.makeDir(path, filesystem)
end

function devfs.setfs(path)
	lib.log.log("devfs", "setFS: Making folder at ", path)
	devfs.makeDir(path)
	for i,v in pairs(fs.list(path)) do
		if v ~= "doc" then
		local f, err = dofile(path.."/"..v)
			if f then
				Internal.write(f, fs.combine(path, v:match("[^%.]+")), filesystem)
				print("Successfully loaded", v, fs.combine(path, v))
			else
				print("Failed to load", v, err)
			end
		end
	end
end

return devfs
