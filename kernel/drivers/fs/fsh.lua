--[[
	VFS helper and moumting manager
	Author: Wassil Janssen a.k.a. Creator
]]--

--local 

--Variables
local altFS = ...
local mounts = {}
local Internal = {}
local filesystems = {}
local fsh = {}

--Functions
function Internal.getFirstElement(path)
	if not type(path) == "string" then error("This is not a string: "..tostring(path),2) end
	return string.sub(path,1,string.find(path,"/") and string.find(path,"/")-1 or -1)
end

function Internal.makeTable(path,tabl)
	if type(path) ~= "string" then error("Expected string, got "..type(path).."!",2) end
	if type(tabl) ~= "table" then error("Expected table, got "..type(tabl).."!",2) end
	path = fs.combine("",path)
	local first = Internal.getFirstElement(path)
	if first == path then
		return tabl, first
	else
		if not tabl[first] then tabl[first] = {} end
		return Internal.makeTable(path:sub(path:find("/")+1,-1),tabl[first])
	end
end

--[[function fsh.mount(from, to, fss)
	fss = fss or "ccfs"
	if not fs.exists(from) then fs.makeDir(from) end
	if not fs.exists(to) then fs.makeDir(to) end
	local tabl, dir = Internal.makeTable(from, mounts)
	tabl[dir] = to
	if fss and fss ~= "ccfs" then
		local tabl, dir = Internal.makeTable(from, filesystems)
		tabl[dir] = fss
	end
end]]--

function fsh.setfs(path, fss)
	if not fs.isDir(path) then fs.makeDir(path) end
	if fss ~= "ccfs" then
		altFS[fss].setfs(path)
		local tabl, dir = Internal.makeTable(path,filesystems)
		tabl[dir] = fss
	end
end

function fsh.link(from, to)
	if not fs.exists(from) then fs.makeDir(from) end
	if not fs.exists(to) then fs.makeDir(to) end
	local tabl, dir = Internal.makeTable(from, mounts)
	tabl[dir] = to
end

function Internal.resolveLinks(path,tabl)
	local first = Internal.getFirstElement(path)
	if tabl[first] then
		if type(tabl[first]) == "table" then
			if first == path then return false end
			return Internal.resolveLinks(path:sub(#first+2,-1),tabl[first])
		elseif type(tabl[first]) == "string" then
			return tabl[first].."/"..path:sub(#first+2,-1)
		end
	end
	return false
end

function Internal.intermediateResolveLinks(path)
	local pathBuffer = path
	repeat
		pathBuffer = fs.combine("",path)
		path = Internal.resolveLinks(pathBuffer,mounts)
	until path == false
	return pathBuffer
end

function fsh.resolveLinks(path)
	if type(path) ~= "string" then
		--print(path)
		error(path,3)
	end
	local toReturn =  Internal.intermediateResolveLinks(fs.combine("", path))
	return toReturn
end

function Internal.getfs(path,tabl)
	local first = Internal.getFirstElement(path)
	if tabl[first] then
		if type(tabl[first]) == "table" then
			return Internal.getfs(path:sub(#first+2,-1),tabl[first])
		elseif type(tabl[first]) == "string" then
			return tabl[first]
		end
	end
	return false
end

function fsh.getfs(path)
	resolved = Internal.getfs(
		fs.combine("", path), filesystems)
	return resolved or "ccfs"
end

return fsh