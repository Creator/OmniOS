--[[
	TMPFS driver developed by Wassil Janssen a.k.a. Creator
]]--

lib = ...
tmpfs = {}
local Internal = {}
filesystem = {}

function Internal.isDir(path,tabl)
	path = fs.combine("",path)
	local first = Internal.getFirstElement(path)
	if first == path and tabl[first] then
		if type(tabl[first]) == "table" then
			return true
		elseif type(tabl[first]) == "string" then
			return false
		end
	elseif tabl[first] then
		if type(tabl[first]) == "table" then
			return Internal.isDir(string.sub(path,#first + 2,-1),tabl[first])
		elseif type(tabl[first]) == "string" then
			return false
		end
	end
	return false
end

function Internal.getFirstElement(path)
	if not type(path) == "string" then error("This is not a string: "..tostring(path),2) end
	return string.sub(path,1,string.find(path,"/") and string.find(path,"/")-1 or -1)
end

function Internal.exists(path,tabl)
	path = fs.combine("",path)
	local first = Internal.getFirstElement(path)
	if tabl[first] then
		if type(tabl[first]) == "table" then
			if first == path then return true end
			return Internal.exists(string.sub(path,#first + 2,-1),tabl[first])
		elseif type(tabl[first]) == "string" and first == path then
			return true
		end
	end
	return false
end

function Internal.read(path,tabl)
	path = fs.combine("",path)
	local first = Internal.getFirstElement(path)
	if tabl[first] then
		if type(tabl[first]) == "table" then
			if first == path then return false end
			return Internal.read(string.sub(path,#first + 2,-1),tabl[first])
		elseif type(tabl[first]) == "string" then
			if first == path then
				return tabl[first]
			else
				return false
			end
		end
	end
	return false
end

function Internal.write(data,path,tabl)
	path = fs.combine("",path)
	local first = Internal.getFirstElement(path)
	if tabl[first] then
		if type(tabl[first]) == "table" then
			if first == path then return false end
			return Internal.write(data,string.sub(path,#first + 2,-1),tabl[first])
		elseif type(tabl[first]) == "string" then
			if first == path then
				tabl[first] = data
			else
				return false
			end
		end
	end
	return false
end

function Internal.delete(path,tabl)
	path = fs.combine("",path)
	local first = Internal.getFirstElement(path)
	if tabl[first] then
		if type(tabl[first]) == "table" then
			if first == path then
				tabl[first] = nil
				return true
			end
			return Internal.delete(string.sub(path,#first + 2,-1),tabl[first])
		elseif type(tabl[first]) == "string" then
			if first == path then
				tabl[first] = nil
			else
				return false
			end
		end
	end
	return false
end

function Internal.list(path,tabl)
	--lib.log.log("ramfs", textutils.serialize(filesystem))
	path = fs.combine("",path)
	local first = Internal.getFirstElement(path)
	if tabl[first] then
		if type(tabl[first]) == "table" then
			if first == path then
				return tabl[first]
			end
			return Internal.list(string.sub(path,#first + 2,-1),tabl[first])
		else
			return false
		end
	end
	return false
end

function Internal.makeDir(path,tabl)
	if path == nil or path == "" then
		return
	end
	lib.log.log("ramfs", "makeDir: Making folder at ", path)
	path = fs.combine("",path)
	local first = Internal.getFirstElement(path)
	lib.log.log("ramfs", "makeDir: First is ", first)
	if tabl[first] then
		lib.log.log("ramfs", "makeDir: ", path, "exists", tabl[first])
		if type(tabl[first]) == "table" then
			if first == path then
				return true
			end
		elseif type(tabl[first]) == "string" then
			return false
		else
			return false
		end
	else
		tabl[first] = {}
		if first == path then return end
	end
	return Internal.makeDir(string.sub(path,#first + 2,-1),tabl[first])
end

--End of internal stuff!

function tmpfs.open(path,mode)
	if mode == "r" then
		if Internal.exists(path,filesystem) and not Internal.isDir(path,filesystem) then
			local data = Internal.read(path,filesystem)
			local index = 1
			local linedData = (function(data)
				local buffer = {}
				for token in string.gmatch(data,"[^\n]+") do
					buffer[#buffer+1] = token
				end
				return buffer
			end)(data)
			local handle = {}
			function handle.readAll()
				return data
			end
			function handle.readLine()
				toRet = linedData[index]
				if not linedData[index] then
					index = 1
				end
				index = index + 1
				return toRet
			end
			function handle.close()
				handle = nil
			end
		else
			return nil
		end
	elseif mode == "w" then
		if Internal.exists(path,filesystem) and not Internal.isDir(path,filesystem) then
			local finalData = ""
			local handle = {}
			function handle.write(data)
				finalData = endData..data
			end
			function handle.writeLine(data)
				finalData = endData..data.."\n"
			end
			function handle.flush()
				Internal.write(finalData,path,filesystem)
			end
			function handle.flush()
				Internal.write(finalData,path,filesystem)
				handle = nil
			end
		else
			return nil
		end
	elseif mode == "a" then
		if Internal.exists(path,filesystem) and not Internal.isDir(path,filesystem) then
			local finalData = Internal.read(path,filesystem)
			local handle = {}
			function handle.write(data)
				finalData = endData..data
			end
			function handle.writeLine(data)
				finalData = endData..data.."\n"
			end
			function handle.flush()
				Internal.write(finalData,path,filesystem)
			end
			function handle.close()
				Internal.write(finalData,path,filesystem)
				handle = nil
			end
		else
			return nil
		end
	elseif mode == "wb" then
		if Internal.exists(path,filesystem) and not Internal.isDir(path,filesystem) then

		else
			return nil
		end
	elseif mode == "rb" then
		if Internal.exists(path,filesystem) and not Internal.isDir(path,filesystem) then

		else
			return nil
		end
	elseif mode == "ab" then
		if Internal.exists(path,filesystem) and not Internal.isDir(path,filesystem) then

		else
			return nil
		end
	else
		return nil
	end
end

function tmpfs.isReadOnly()
	return false
end

function tmpfs.delete(path)
	return Internal.delete(path,filesystem)
end

function tmpfs.isDir(path)
	return Internal.isDir(path,filesystem)
end

function tmpfs.getDrive(path)
	fs.fs.ccfs.getDrive(Internal.getFirstElement(path))
end

function tmpfs.getSize(path)
	if tmpfs.exists(path) and not tmpfs.isDir(path) then
		return #Internal.read(path)
	else
		return 0
	end
end

function tmpfs.exists(path)
	Internal.exists(path,filesystem)
end

function tmpfs.list(path)
	if tmpfs.isDir(path) then
		local final = {}
		for i,v in pairs(Internal.list(path,filesystem)) do
			final[#final+1] = i
		end
		return final
	else
		return false
	end
end

function tmpfs.makeDir(path)
	return Internal.makeDir(path, filesystem)
end

function tmpfs.setfs(path)
	lib.log.log("ramfs", "setFS: Making folder at ", path)
	tmpfs.makeDir(path)
end

return tmpfs

--print(Internal.exists("hi/wow/r/m",filesystem))
--print(Internal.isDir("hi/wow/r",filesystem))
--print(Internal.read("hi/wow/r",filesystem))
--Internal.write("wow","hi/wow/r",filesystem)
--print(Internal.read("hi/wow/r",filesystem))
--Internal.delete("hi/wow",filesystem)
--print(Internal.read("hi/wow/r",filesystem))
--print(text--Utils.serialize(tmpfs.list("hi/wow/",filesystem)))
