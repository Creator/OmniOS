--Code--

local pts, dofile = ...

local lib = {}

pts("Loading libraries...")

for i,v in pairs(fs.list("OmniOS/kernel/lib")) do
	local smth = v:match("([^%.]+)")
	local err = nil
	pts("Loading \027+d@"..smth)
	lib[smth], err = dofile("OmniOS/kernel/lib/"..v, lib)
	if not lib[smth] then
		pts(err)
		lib.log.log("lib", smth, err)
		read()
	end
	read()
end

return lib
