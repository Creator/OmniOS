--[[
	Thread library
]]--

local lib = ...
local nW = lib.window

return function(func, parent, level, env, name,  ...)
	
	if not (type(env) == "table") then
		return false, "Argument env has to be a table."
	end 
	if not (type(func) == "function") then
		return false, "Argument func has to be a function."
	end
	if not (type(level) == "number") then
		return false, "Argument level has to be a number."
	end
	name = name or "Nameless"
	if not (type(name) == "string") then
		return false, "Argument name has to be a string."
	end

	setfenv(func, env)
	local process = coroutine.create(func)
	local filter = nil
	local first, ok, err
	local parent = parent
	local window = nW(parent, 1, 1, 51, 19, false)

	ok, err = coroutine.resume(process, ...)
	if ok then
		filter = err
	else
		print(err)
		return false, err
	end

	--Public
	local self = {}

	function self.isDead()
		return coroutine.status(process) == "dead" and true or false
	end

	function self.kill()

	end

	function self.setVisible(isVisible)
		parent.setVisible(isVisible)
	end

	self.resume = function(...)
		first = ...
		if self.isDead() then
			return false, "Is dead."
		end
		if filter == nil or first == filter then
			ok, err = coroutine.resume(process,...)
			if ok then
				filter = err
				return true
			else
				return false, err
			end
		end
	end



	return self
end
