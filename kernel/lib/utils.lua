--[[
	utils
	by Wassil Janssen a.k.a. Creator
	OmniOS
]]--


function split(toSplit,sep)
	if type(toSplit) ~= "string" then return false end
	sep = sep or "%s"
	local ret = {}
	local count = 1
	for token in toSplit:gmatch("[^"..sep.."]+") do
		ret[count] = token
		count = count + 1
	end
	return ret
end

local function tablecopy(source,destination)
	source = type(source) == "table" and source or {}
	for i,v in pairs(source) do
		if type(v) == "table" then
			destination[i] = {}
			local mt = getmetatable(v)
			if mt and type(mt) == "table" then
				setmetatable(destnation[i],mt)
			end
			tablecopy(v,destination[i])
		else
			destination[i] = v
		end
	end
end

local table = {
	copy = function(source,destination)
		destination = destination or {}
		tablecopy(source,destination)
		return destination
	end
}

return {
	split = split,
	table = table,
}