--[[
	Log API
	by Creator
	for TheOS
]]--

local doLog = true
local logPath = "logs"
local stdout = fs.open(logPath.."/stdout.log","a")
local isOpen = true

local log = {}

local function concat(...)
	local str = ""
	for i,v in pairs({...}) do
		str = str.." "..tostring(v)
	end
	return str:sub(2,-1)
end


function log.log(category, ...)
	local file = fs.open(logPath.."/"..tostring(category)..".log","a")
	file.write(concat(...).."\n")
	file.close()
end

function log.stdout(...)
	if not isOpen then
		stdout = fs.open(logPath.."/stdout.log","a")
	end
	isOpen = true
	stdout.write(concat(...).."\n")
	stdout.flush()
end

function log.stdclose()
	stdout.close()
	isOpen = false
end

return log
