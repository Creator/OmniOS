--[[
	OmniOS Interface for Sandboxed Processes
]]--

local OmniOS = {}

function OmniOS.launch(program,parent,...)

end

function OmniOS.message(thread,message)
  Kernel.addMessage(thread,Kernel.getRunning(),message)
end

function OmniOS.getName(...)
	return Kernel.getName(...)
end
function OmniOS.getRunning(...)
	return Kernel.getRunning(...)
end
function OmniOS.getMessages()
	return Kernel.getMessages(Kernel.getRunning())
end

function OmniOS.broadcast(message)
	Kernel.broadcast(Kernel.getRunning(),message)
end
function OmniOS.switch(id)
	Kernel.switch(id)
end

function OmniOS.list()
	return Kernel.list()
end

return OmniOS