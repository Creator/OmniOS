--[[
	CRC lib for OmniOS
]]

local bnot   = bit32.bnot
local rshift = bit32.rshift
local bxor   = bit32.bxor
local band   = bit32.band
local crc_table = {}
local POLY = 0xEDB88320

local function crc_core(crc)
	for _=1, 8 do
		local b = band(crc, 1)
		crc = rshift(crc, 1)
		if b == 1 then crc = bxor(crc, POLY) end
	end
	return crc
end

local function crc_byte(byte, crc)
	crc = bnot(crc or 0)
	local a = rshift(crc, 8)
	local c = bxor(crc % 256, byte)
	local b = crc_table[c]
	if not b then
		b = crc_core(c)
		crc_table[c] = b
	end
	return bnot(bxor(a, b))
end

local function crc_string(str, crc)
	crc = crc or 0
	for i=1, #str do
		crc = crc_byte(str:byte(i,i), crc)
	end
	return crc
end

local function crc32(str, crc)
	if type(str) == "string" then
		return crc_string(str, crc)
	else
		return crc_byte(str, crc)
	end
end

return crc32