--[[
	Sandbox
	environment
	by Creator
	for OmniOS
]]--

local lib = ...

local default = {}
local exceptions = {}
local oldGetfenv = getfenv

--getfenv(rs.get)

local function nCopy(t, nt, recr, env)
	if recr[tostring(t)] then
		for i,v in pairs(t) do
			nt[i] = v
		end
		return
	end
	for i,v in pairs(t) do
		if type(v) == "table" then
			local nnt = {}
			recr[tostring(v)] = nnt
			nt[i] = nnt
			nCopy(v, nnt, recr)
		else
			nt[i] = v
		end
	end
end

local function copy(t, env)
	if type(t) == "table" then
		local nTable = {}
		nCopy(t, nTable, {}, env)
		return nTable
	else
		return t
	end
end

for i, v in pairs(_G) do
	default[i] = true
end


local function generateGet(ret)
	local ret = ret
	return function(env)
		if env == nil then 
			return ret 
		elseif type( env ) == "number" and env > 0 then 
			env = env + 1 
		end 
		local fenv = getfenv(env)
		if fenv == _G then
			return ret
		end
		return fenv
	end
end

local function sandbox(include)
	local ret = {}
	for i,v in pairs(default) do
		if i == "_G" then
			ret._G = ret
		elseif i == "getfenv" then
			ret[i] = generateGet(ret)
		else
			ret[i] = copy(_G[i], ret)
		end
	end
	if type(include) == "table" then
		for i,v in pairs(include) do
			ret[i] = copy(include[i], ret)
		end
	end

	return ret
end

return sandbox