--[[
	New OmniOS Kernel
	by Creator
]]--

local lib, dofile, pts, path = ...

local kernel = {}
local active = 1
local processes = {}
local windows = {}
local daemons = {}
local eventFilter = {["key"] = true, ["mouse_click"] = true, ["paste"] = true,["char"] = true, ["terminate"] = true, ["mouse_scroll"] = true, ["mouse_drag"] = true, ["key_up"] = true, ["mouse_up"] = true}
local eventBuffer = {}
local running = active
local nW = lib.window
local internal = {}
local currTerm = term.current()
local tasks = {}
local w,h = term.getSize()
local skipEvent = false
local ids = {}
local hooks = {}
local procWait = {}

--[[Utils]]

local function removeFirst(_, ...)
	return ...
end

function internal.queueTask(...)
	tasks[#tasks+1] = {...}
end

function internal.fKill(id)
	if tonumber(id) and processes[id] then
		--lib.log.stdout("FInally killed process #", id)
		processes[id] = nil
	end
end

function internal.kill(id)
	if id == 1 then
		return
	elseif id == active then
		kernel.switch(1)
	end
	internal.queueTask("fKill", id)
end

function internal.resume(id)
	processes[id].resume()
end

local function getFirst(t)
	local f = t[1]
	if #t > 1 then
		for i=1, #t - 1 do
			t[i] = t[i + 1]
			t[i + 1] = nil
		end
	else
		t[1] = nil
	end
	--lib.log.stdout("First", f, #t, t[1])
	for i,v in pairs(t) do
		--lib.log.stdout("wheh", i, v)
	end
	return f, t
end

function internal.newPipe(toID)
	local queue = {}
	local reads = 0
	local inputIsDead = false
	local pipe = {
		write = function(...)
		--lib.log.stdout(... , toID)
			kernel.resume(toID)
			for i,v in pairs({...}) do
				queue[#queue+1] = v
			end
			if processes[toID] and processes[toID].waiting then
				processes[toID].waiting = false
				processes[toID].ready = true
			end
		end,
		read = function()
			if #queue == 0 then
				reads = reads + 1
				--lib.log.stdout("0 elemants in the queue. Reads ", reads)
				if reads > 10 then
					return nil
				end
				if inputIsDead then
					--lib.log.stdout("Input is real dead")
					return nil
				else
					processes[toID].waiting = true
					coroutine.yield()
					local f, queue = getFirst(queue)
					return f
				end
			else
				local f, queue = getFirst(queue)
				--lib.log.stdout("First from getFirst is ", f)
				return f
			end
		end,
		deathHandle = function()
			--lib.log.stdout("Input died")
			inputIsDead = true
		end
	}
	return pipe
end

function internal.newProc(func, parent, level, id, name, env, info, ...)
	env = env or lib.sandbox({})
	if not (type(func) == "function") then
		return false, "Argument func has to be a function."
	end
	if not (type(level) == "number") then
		return false, "Argument level has to be a number."
	end
	if not (type(id) == "number") then
		return false, "Argument level has to be a number."
	end
	name = name or "Nameless"
	if not (type(name) == "string") then
		return false, "Argument name has to be a string."
	end

	setfenv(func, env)
	
	local process = coroutine.create(func)
	local filter = nil
	local first, ok, err
	local parent = parent

	--Public
	local self = {}

	self.info = info

	function self.isDead()
		local sts = coroutine.status(process)
		if sts == "dead" then
			if info.pipe then
				--lib.log.stdout("Closing pipe from id ", id)
				info.pipe()
			end
			if info.getInput then
				processes[info.getInputFrom].redirectingEventsTo = false
			end
			if info.onDeath then
				info.onDeath()
			end
			return true
		end
	end

	function self.setVisible(isVisible)
		window.setVisible(isVisible)
	end

	function self.resume(...)
		first = ...

		if self.redirectingEventsTo and processes[self.redirectingEventsTo] then
			--lib.log.stdout("Process ", id, " is sending output to ", self.redirectingEventsTo)
			processes[self.redirectingEventsTo].resume(...)
			return
		end

		if self.isDead() then
			kernel.kill(id)
			return
		end

		if filter == nil or first == filter then
			--Window switching magic
			running = id
			
			term.redirect(windows[info.windowID])
			if info.windowID == active then
				--lib.log.log("window", name, "has process id", id, "and window id", info.windowID, windows[info.windowID], windows[id], currTerm)
				windows[info.windowID].setVisible(true)
				--windows[info.windowID].redraw()
				windows[info.windowID].restoreCursor()
			end

			--lib.log.log("varagr", id, name, ...)
			ok, err = coroutine.resume(process, ...)
			term.redirect(currTerm)

			--window.setVisible(false)

			if self.isDead() or not ok then
				lib.log.log("Error", id, name, tostring(err).."\n")
				kernel.kill(id)
				return
			else
				filter = err
			end
		else
			return true
		end
	end

	function self.redraw()
		windows[info.windowID].setVisible(true)
		windows[info.windowID].redraw()
		windows[info.windowID].setVisible(false)
	end

	function self.getName()
		return name
	end

	function self.getLevel()
		return level
	end

	function self.getStatus()
		return coroutine.status(process)
	end

	self.waiting = false
	self.ready = false

	--lib.log.log("varagr", id, name, ...)
	self.resume(...)

	processes[id] = self
end


function internal.switch(id)
	if processes[id] then
		active = id
		processes[id].redraw()
	end
end

function internal.skipEvent(bool)

end

function internal.drawOpen()
	term.redirect(currTerm)
	term.setCursorBlink(false)
	paintutils.drawFilledBox(w-15,1,w,h,colors.black)
	term.setTextColor(colors.white)
	local list = kernel.list()
	local max = 1
	for i, v in pairs(list) do
		if active == v[1] then paintutils.drawLine(w-15, i, w, i, colors.blue) end
		term.setCursorPos(w-14, i)
		term.setBackgroundColor(active == v[1] and colors.blue or colors.black)
		term.write("x "..v[1].." "..v[2])
		max = i
	end

	while true do
		local event = {coroutine.yield()}
		if event[1] == "mouse_click" then
			if event[3] < w-15 then
				break
			elseif event[3] == w-14 and event[4] <= max then
				kernel.kill(list[event[4]][1])
				--if links[evnt[4]] == active then break end
				break
			elseif event[3] >= w-12 and event[4] <= max then
				kernel.switch(list[event[4]][1])
				break
			end
		elseif event[1] == "char" then
			local tn = tonumber(event[2])
			if tn then
				if tn == 0 then
					kernel.switch(10)
					break
				else
					kernel.switch(tn)
					break
				end
			else
				eventBuffer[#eventBuffer+1] = event
			end
		elseif event[1] == "key" then
			local tn = event[2]
			if tn == 56 or tn == 29 then
				break
			else
				eventBuffer[#eventBuffer+1] = event
			end
		elseif not (event[1] == "mouse_up" or event[1] == "mouse_drag") then
			eventBuffer[#eventBuffer+1] = event
		end
	end
	if processes[active] then
		processes[active].redraw()
	end
end

--Kernel Stuff

function kernel.newPipe(...)
	return internal.newPipe(...)
end

function kernel.newProc(func, parent, level, name, add, ...)
	local id = 1
	local env = {}
	local info = {}
	while true do
		if not processes[id] then break end
		id = id + 1
	end
	--lib.log.log("varagr", id, tostring(name).."plc", ...)
	if level < processes[running].getLevel() then
		level = processes[running].getLevel()
	end

	parent = type(parent) == "table" and parent or currTerm

	if level == 0 then
		env = lib.sandbox({["kernel"] = kernel, ["internal"] = internal, OmniOS = lib.OmniOS})
	elseif level == 1 then
		env = lib.sandbox({kernel = kernel, OmniOS = lib.OmniOS})
	else
		env = lib.sandbox({OmniOS = lib.OmniOS})
	end

	if type(add) == "table" then
		for i,v in pairs(add) do
			env[i] = v
		end
	end

	local handle = {
		stdout = function(pout)
			env["write"] = pout
			env["print"] = pout
			env.term["write"] = pout
		end,
		stdin = function(pin)
			env["read"] = pin
		end,
		set = function(a,b)
			if not type(a) == "string" then
				return false
			end
			env[a] = b
		end,
		pipe = function(p)
			info.pipe = p
		end,
		getOutput = function(b)
			if b then
				info.windowID = running
			else
				info.windowID = id
			end
		end,
		sendEvents = function(yesorno)
			--lib.log.stdout("Process ",running, " to ", id)
			if yesorno then
				info.getInput = true
				info.getInputFrom = running
				processes[running].redirectingEventsTo = id
			else
				info.getInput = false
				info.getInputFrom = false
				processes[running].redirectingEventsTo = false
			end
		end,
		onDeath = function(func)
			info.onDeath = func
		end
	}

	info.windowID = id
	windows[id] = nW(parent, 1, 1, w, h, false)
	processes[id] = {}
	internal.queueTask("newProc", func, parent, level, id, name, env, info, ...)
	return id, handle
end

function kernel.list()
	local ret = {}
	for i,v in pairs(processes) do
		ret[#ret+1] = {i, processes[i].getName()}
	end
	return ret
end

function kernel.switch(id)
	internal.queueTask("switch", id)
end

function kernel.getpid()
	return running
end

function kernel.kill(id)
	if tonumber(id) and id > 1 then
		internal.queueTask("kill", id)
	end
end

function kernel.getRunning()
	return running
end

function kernel.getName(id)
	return processes[id] and processes[id].getName and processes[id].getName() or false
end

function kernel.addHook(eventName, func)
	if not (type(eventName) == "string") then
		return false, "<eventName> is no string."
	end
	if not (type(func) == "function" or type(func) == "thread") then
		return false, "<func> is no string."
	end

	if not hooks[eventName] then
		hooks[eventName] = {}
	end
	hooks[eventName][#hooks[eventName]+1] = func
end

function kernel.resume(id)
	local nID = tonumber(id)
	if nID then
		if processes[nID] then
			internal.queueTask("resume", nID)
		end
	end

end

--[[Initializations]]

--[[Shell]]
local file = fs.open(path, "r")
local data = file.readAll()
file.close()
local sh, err = loadstring(data, "OmniOS Shell")
if not sh then
	print(err)
	read()
	error(err)
end

--[[FileX]]
local file = fs.open("OmniOS/programs/FileX/main.lua", "r")
local data = file.readAll()
file.close()
local fx = loadstring(data, "FileX")

--[[Hooks]]

local function keySwitch(_event,_code)
	local time = 0
	while true do
		if _event == "key" and _code == 56 then
			time = os.clock()
		elseif _event == "char" and tonumber(_code) then
			if os.clock() - time <= 0.50 then
				kernel.switch(tonumber(_code))
			end
		end
		_event, _code = coroutine.yield()
	end
end

local keySwitchCoro = coroutine.create(keySwitch)

local function openProcList(_event, _code)
	local time = 0
	local key = ""
	while true do
		if _event == "key" and _code == 56 then
			if os.clock() - time <= 0.50 and key == "ctrl" then
				time = 0
				internal.queueTask("drawOpen")
				key = ""
			else
				time = os.clock()
				key = "alt"
			end
		elseif _event == "key" and _code == 29 and key == "alt" then
			if os.clock() - time <= 0.50 then
				time = 0
				internal.queueTask("drawOpen")
				key = ""
			else
				time = os.clock()
				key = "ctrl"
			end
		end
		_event, _code = coroutine.yield()
	end
end

local openProcListCoro = coroutine.create(openProcList)

kernel.addHook("key", keySwitchCoro)
kernel.addHook("char", keySwitchCoro)
kernel.addHook("key", openProcListCoro)

--internal.newProc(func, parent, level, id, name, ...)
windows[1] = nW(currTerm, 1, 1, w, h, false)
windows[2] = nW(currTerm, 1, 1, w, h, false)
internal.newProc(sh, term.current(), 0, 1, "Shell", lib.sandbox({["kernel"] = kernel, ["sandbox"] = lib.sandbox, ["log"] = lib.log, ["lib"] = lib}), {windowID = 1}, ...)
internal.newProc(fx, term.current(), 1, 2, "FileX", lib.sandbox({["kernel"] = kernel}), {windowID = 2}, ...)

--[[Initializations end]]

--kernel.switch(2)

while true do
	while not (#tasks == 0) do
		local first = table.remove(tasks,1)
		if internal[first[1]] then
			--lib.log.stdout("Executing internal task ", unpack(first))
			internal[first[1]](removeFirst(unpack(first)))
		end
	end
	local event = #eventBuffer == 0 and {coroutine.yield()} or table.remove(eventBuffer, 1)

	if hooks[event[1]] then
		for i, v in pairs(hooks[event[1]]) do
			if type(v) == "function" then
				v(unpack(event))
			elseif type(v) == "thread" then
				local ok, err = coroutine.resume(v, unpack(event))
				if not ok then
					lib.log.stdout(err)
				end
			end
		end
	end

	if eventFilter[event[1]] then
		if not processes[active].waiting and not processes[active].info.getInput then
			processes[active].resume(unpack(event))
		end
	else
		for i,v in pairs(processes) do
			if not processes[i].waiting and not processes[i].info.getInput then
				processes[i].resume(unpack(event))
			end
		end
	end

	for i,v in pairs(processes) do
		if processes[i].ready then
			processes[i].ready = false
			processes[i].resume()
		end
	end
end