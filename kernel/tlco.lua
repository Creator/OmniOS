local function dofile(path,...)
	local func, err = loadfile(path)
	if func then
		setfenv(func,_G)
		return pcall(func, ...)
	else
		return false, err
	end
end

local a = _G.os.shutdown
function _G.os.shutdown()
	_G.os.shutdown=a
	term.redirect(term.native())
	multishell=nil
	ok, err = dofile("OmniOS/kernel/boot.lua")
	local file = fs.open("logs/boot.log","a")
	file.write(err.."\n")
	file.close()
end

os.queueEvent("modem_message", {})
os.pullEvent()
os.queueEvent("key")
