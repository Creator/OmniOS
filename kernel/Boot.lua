term.clear()
term.setCursorPos(1,1)
term.redirect(term.native())

local function printf(...)
	 --[[
		Author: Wassil Janssen a.k.a. Creator
	 ]]--
	local clrs = {
		["0"] = colors.white,
		["1"] = colors.orange,
		["2"] = colors.magenta,
		["3"] = colors.lightBlue,
		["4"] = colors.yellow,
		["5"] = colors.lime,
		["6"] = colors.pink,
		["7"] = colors.gray,
		["8"] = colors.lightGray,
		["9"] = colors.cyan,
		["a"] = colors.purple,
		["b"] = colors.blue,
		["c"] = colors.brown,
		["d"] = colors.green,
		["e"] = colors.red,
		["f"] = colors.black,
	}
	local prefixes = {
		["+"] = term.setTextColor,
		["-"] = term.setBackgroundColor,
	}
	local str = ""
	for i,v in pairs({...}) do
		str = str.." "..tostring(v)
	end
	str = str:sub(2,-1)
	local pos = 1
	local toPrint = ""
	while true do
		local skip = 0
		local char = str:sub(pos,pos)
		if char == "\027" then
			term.write(toPrint)
			toPrint = ""
			local n = str:sub(pos+1,pos+1)
			local nn = str:sub(pos+2,pos+2)
			if prefixes[n] and clrs[nn] then
				prefixes[n](clrs[nn])
				skip = skip +2
				if str:sub(pos+3,pos+3) ~= "@" then
					local n = str:sub(pos+3,pos+3)
					local nn = str:sub(pos+4,pos+4)
					if prefixes[n] and clrs[nn] then
						prefixes[n](clrs[nn])
						skip = skip +2
					end
				else
					skip = skip + 1
				end
			end
		elseif char == "\n" then
			print("")
		else
			toPrint = toPrint..char
		end
		if pos == #str then break end
		pos = pos + 1 + skip
	end
	if toPrint ~= "" then
		term.write(toPrint)
	end
end


--[[
--Original read function by DanTwoHundred. This version is slightly modified to fit the needs of OmniOS.

local oldTermWrite = term.write

function read( _sReplaceChar, _tHistory, _fnComplete )
	term.setCursorBlink( true )

	local sLine = ""
	local nHistoryPos
	local nPos = 0
	if _sReplaceChar then
		_sReplaceChar = string.sub( _sReplaceChar, 1, 1 )
	end

	local tCompletions
	local nCompletion
	local function recomplete()
		if _fnComplete and nPos == string.len(sLine) then
			tCompletions = _fnComplete( sLine )
			if tCompletions and #tCompletions > 0 then
				nCompletion = 1
			else
				nCompletion = nil
			end
		else
			tCompletions = nil
			nCompletion = nil
		end
	end

	local function uncomplete()
		tCompletions = nil
		nCompletion = nil
	end

	local w = term.getSize()
	local sx = term.getCursorPos()

	local function redraw( _bClear )
		local nScroll = 0
		if sx + nPos >= w then
			nScroll = (sx + nPos) - w
		end

		local cx,cy = term.getCursorPos()
		term.setCursorPos( sx, cy )
		local sReplace = (_bClear and " ") or _sReplaceChar
		if sReplace then
			oldTermWrite( string.rep( sReplace, math.max( string.len(sLine) - nScroll, 0 ) ) )
		else
			oldTermWrite( string.sub( sLine, nScroll + 1 ) )
		end

		if nCompletion then
			local sCompletion = tCompletions[ nCompletion ]
			local oldText, oldBg
			if not _bClear then
				oldText = term.getTextColor()
				oldBg = term.getBackgroundColor()
				term.setTextColor( colors.white )
				term.setBackgroundColor( colors.gray )
			end
			if sReplace then
				oldTermWrite( string.rep( sReplace, string.len( sCompletion ) ) )
			else
				oldTermWrite( sCompletion )
			end
			if not _bClear then
				term.setTextColor( oldText )
				term.setBackgroundColor( oldBg )
			end
		end

		term.setCursorPos( sx + nPos - nScroll, cy )
	end
	
	local function clear()
		redraw( true )
	end

	recomplete()
	redraw()

	local function acceptCompletion()
		if nCompletion then
			-- Clear
			clear()

			-- Find the common prefix of all the other suggestions which start with the same letter as the current one
			local sCompletion = tCompletions[ nCompletion ]
			sLine = sLine .. sCompletion
			nPos = string.len( sLine )

			-- Redraw
			recomplete()
			redraw()
		end
	end
	while true do
		local sEvent, param = os.pullEvent()
		if sEvent == "char" then
			-- Typed key
			clear()
			sLine = string.sub( sLine, 1, nPos ) .. param .. string.sub( sLine, nPos + 1 )
			nPos = nPos + 1
			recomplete()
			redraw()

		elseif sEvent == "paste" then
			-- Pasted text
			clear()
			sLine = string.sub( sLine, 1, nPos ) .. param .. string.sub( sLine, nPos + 1 )
			nPos = nPos + string.len( param )
			recomplete()
			redraw()

		elseif sEvent == "key" then
			if param == keys.enter then
				-- Enter
				if nCompletion then
					clear()
					uncomplete()
					redraw()
				end
				break
				
			elseif param == keys.left then
				-- Left
				if nPos > 0 then
					clear()
					nPos = nPos - 1
					recomplete()
					redraw()
				end
				
			elseif param == keys.right then
				-- Right                
				if nPos < string.len(sLine) then
					-- Move right
					clear()
					nPos = nPos + 1
					recomplete()
					redraw()
				else
					-- Accept autocomplete
					acceptCompletion()
				end

			elseif param == keys.up or param == keys.down then
				-- Up or down
				if nCompletion then
					-- Cycle completions
					clear()
					if param == keys.up then
						nCompletion = nCompletion - 1
						if nCompletion < 1 then
							nCompletion = #tCompletions
						end
					elseif param == keys.down then
						nCompletion = nCompletion + 1
						if nCompletion > #tCompletions then
							nCompletion = 1
						end
					end
					redraw()

				elseif _tHistory then
					-- Cycle history
					clear()
					if param == keys.up then
						-- Up
						if nHistoryPos == nil then
							if #_tHistory > 0 then
								nHistoryPos = #_tHistory
							end
						elseif nHistoryPos > 1 then
							nHistoryPos = nHistoryPos - 1
						end
					else
						-- Down
						if nHistoryPos == #_tHistory then
							nHistoryPos = nil
						elseif nHistoryPos ~= nil then
							nHistoryPos = nHistoryPos + 1
						end                        
					end
					if nHistoryPos then
						sLine = _tHistory[nHistoryPos]
						nPos = string.len( sLine ) 
					else
						sLine = ""
						nPos = 0
					end
					uncomplete()
					redraw()

				end

			elseif param == keys.backspace then
				-- Backspace
				if nPos > 0 then
					clear()
					sLine = string.sub( sLine, 1, nPos - 1 ) .. string.sub( sLine, nPos + 1 )
					nPos = nPos - 1
					recomplete()
					redraw()
				end

			elseif param == keys.home then
				-- Home
				if nPos > 0 then
					clear()
					nPos = 0
					recomplete()
					redraw()
				end

			elseif param == keys.delete then
				-- Delete
				if nPos < string.len(sLine) then
					clear()
					sLine = string.sub( sLine, 1, nPos ) .. string.sub( sLine, nPos + 2 )                
					recomplete()
					redraw()
				end

			elseif param == keys["end"] then
				-- End
				if nPos < string.len(sLine ) then
					clear()
					nPos = string.len(sLine)
					recomplete()
					redraw()
				end

			elseif param == keys.tab then
				-- Tab (accept autocomplete)
				acceptCompletion()

			end

		elseif sEvent == "term_resize" then
			-- Terminal resized
			w = term.getSize()
			redraw()

		end
	end

	local cx, cy = term.getCursorPos()
	term.setCursorBlink( false )
	term.setCursorPos( w + 1, cy )
	print()
	
	return sLine
end
]]

local counter = 0

local function pts(str)
	printf("\027+1@["..tostring(counter).."] \027+b@"..tostring(str))
	counter = counter + 1
	print("")
end

local function handle(check, ...)
	if check then
		return ...
	end
	print(...)
	read()
	return false, ...
end

function _G.dofile(path,...)
	local func, err = loadfile(path)
	if func then
		setfenv(func,_G)
		return handle(pcall(func, ...))
	else
		return false, err
	end
end

pts("Loading OmniOS into memory...")

local lib = dofile("OmniOS/kernel/libload.lua", pts, dofile)

pts("Finished loading kernel libs...")

pts("Now loading drivers into memory...")

dofile("OmniOS/kernel/drivers/drivers.lua", pts, dofile, lib)

pts("Drivers were successfully loaded into memory...")

local status, err = dofile("OmniOS/kernel/kernel.lua", lib, dofile, pts, "OmniOS/bin/shell.lua")
print(err)
read()
--dofile("OmniOS/programs/FileX/main.lua")

read()