--[[
  Author: Wassil Janssen a.k.a. Creator
]]--
local function toRet(...)
  local monTable = (...)
  assert(type(monTable) == "table","Expected table!",2)

  local num = ""
  local x,y,isColor = 0,0,0
  local temp = {x = 0,y = 0}

  for i,v in pairs(monTable) do
    assert(type(v) == "table","Expected table!",2)
    if num == "" then
      num = #v
    elseif #v ~= num then
      error("Table is invalid",2)
    end
  end
  for i,v in pairs(monTable) do
    for k,m in pairs(v) do
      assert(peripheral.isPresent(m),"The monitor "..m.." does not exist!",2)
      assert(peripheral.getType(m) == "monitor","Not a monitor, sorry!",2)
      if x == 0 and y == 0 then
        x,y = peripheral.call(m,"getSize")
      else
        temp.x,temp.y = peripheral.call(m,"getSize")
        assert(temp.x == x and temp.y == y,"The monitors don't have the same size!",2)
      end
      if isColor == 0 then
        isColor = peripheral.call(m,"isColor")
      else
        assert(isColor == peripheral.call(m,"isColor"),"The monitors are not of the same type!",2)
      end
    end
  end
  --Check done!
  --Functions
  local monitors = {}
  local handle = {}
  local x,y = 1,1
  local w,h = peripheral.call(monTable[1][1],"getSize")
  local isBlink = false

  for i,v in pairs(monTable) do
    monitors[#monitors+1] = (function()
      local ret = {}
      for k,m in pairs(v) do
        ret[#ret+1] = peripheral.wrap(m)
      end
      return ret
    end)()
  end

  handle.isColor = monitors[1][1].isColor
  handle.isColour = monitors[1][1].isColor

  function handle.getSize()
    return w*#monitors[1],h*#monitors
  end

  function handle.setCursorPos(tx,ty)
    x = tx
    y = ty
    handle.setCursorBlink(isBlink)
  end

  function handle.setTextScale(scale)
    for i,v in pairs(monitors) do
      for k,m in pairs(v) do
        m.setTextScale(scale)
      end
    end
    w,h = monitors[1][1].getSize()
  end

  function handle.clear()
    for i,v in pairs(monitors) do
      for k,m in pairs(v) do
        m.clear()
      end
    end
  end

  function handle.setTextColor(color)
    for i,v in pairs(monitors) do
      for k,m in pairs(v) do
        m.setTextColor(color)
      end
    end
  end

  handle.setTextColour = handle.setTextColor

  function handle.setBackgroundColor(color)
    for i,v in pairs(monitors) do
      for k,m in pairs(v) do
        m.setBackgroundColor(color)
      end
    end
  end

  handle.setBackgroundColour = handle.setBackgroundColor

  function handle.scroll(lines)
    for i,v in pairs(monitors) do
      for k,m in pairs(v) do
        m.scroll(lines)
      end
    end
  end

  function handle.setCursorBlink(blink)
    for i,v in pairs(monitors) do
      for k,m in pairs(v) do
        m.setCursorBlink(false)
      end
    end
    local whichY = math.ceil(y/h)
    local whichX = math.ceil(x/w)
    monitors[whichY][whichX].setCursorPos(x%w,y%h)
    monitors[whichY][whichX].setCursorBlink(blink)
    isBlink = blink
  end

  function handle.clearLine()
    local whichY = math.ceil(y/h)
    local height = y%h
    local whichX = math.ceil(x/w)
    for i,v in pairs(monitors[whichY]) do
      if i == whichX then
        v.setCursorPos(x%w,height)
        v.clearLine()
      else
        v.setCursorPos(1,height)
        v.clearLine()
      end
    end
  end

  function handle.getCursorPos()
    return x,y
  end

  function handle.write(text)
    local tempX = x
    local tempBlink = isBlink
    handle.setCursorBlink(false)
    text = tostring(text)
    for i = 1, text:len() do
      local whichY = math.ceil(y/h)
      local height = y%h
      local whichX = math.ceil(tempX/w)
      local width = tempX%w
      handle.setCursorPos(tempX,y)
      monitor[whichY][whichX].write(text:sub(i,i))
      tempX = tempX + 1
    end
    handle.setCursorPos(tempX+1,y)
    x = tempX
    handle.setCursorBlink(tempBlink)
  end

  return handle
end
return toRet
