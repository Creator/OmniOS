--[[
  Author: Wassil Janssen a.k.a. Creator
]]--

local per = Utils.table.copy(peripheral)
local extra = {}
for i,v in pairs(fs.list("OmniOS/Drivers/peripheral")) do
  extra[v:match("[^%.]+")] = dofile("OmniOS/Drivers/peripheral/"..v)
end

function peripheral.wrap(side,...)
  if extra[side] then
    return extra[side](...)
  else
    return per.wrap(side)
  end
end
