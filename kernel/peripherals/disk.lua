local function mountDisk(event,side)
	if not disk.hasAudio(side) then
			fs.link("OmniOS/usr/"..tostring(disk.getMountPath(side)),tostring(disk.getMountPath(side)))
	end
end

for i,side in pairs(peripheral.getNames()) do
	if peripheral.getType(side) == "drive" then
		if not disk.hasAudio(side) and disk.getMountPath(side) then
				fs.link("OmniOS/usr/"..tostring(disk.getMountPath(side)),tostring(disk.getMountPath(side)))
		end
	end
end

Kernel.bindEvent("disk",mountDisk)
