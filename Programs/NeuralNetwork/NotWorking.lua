--Neural Network by Creator a.k.a. Wassil Janssen
local eta = 0.15
local alpha = 0.5
--Neuron
local Neuron = {
  new = function(numInputs, first)
    local self = {
      output = 0,
      weights = {},
      gradient = 0
    }
    for i=(first and 1 or 0),numInputs do
      self.weights[i] = 1/(numInputs+(first and 0 or 1))
    end
    setmetatable(self,{__index = Neuron})
    print("Made neuron with "..tostring(numInputs).." inputs!")
    return self
  end,

  run = function(self,input)
    local sum = 0
    for i=0,#input do
      sum = sum + self.weights[i]*input[i]
    end
    self.output = 1 / (1 + math.exp((sum * -1) / 0.5))
    print(self.output)
    return self.output
  end,

  derivativeTransferFunction = function(x)
    return 1.0 - x * x
  end,

  calculateOutputGradient = function(self,target)
    local delta = target - self.output
    self.gradient = delta * (1.0 - self.output*self.output)
  end,

  SumDOW  = function(nextLayer,index)
    local sum = 0
    for i=1,#nextLayer do
      sum = sum + nextLayer[i].weights[index] * nextLayer[i].gradient
    end
    return sum
  end,

  calcHiddenGradient = function(self,nextLayer,index)
    local dow = (function(nextLayer,index)
      local sum = 0
      for i=1,#nextLayer do
        sum = sum + nextLayer[i].weights[index] * nextLayer[i].gradient
      end
      return sum
    end)(nextLayer,index)
    self.gradient = dow * (1.0 - self.output*self.output)
  end,

  updateInputWeights = function(self,prevLayer)
    for i=0,#prevLayer do
      local n = prevLayer[i]
      local oldDeltaWeight = self.weights[i]
      local newDeltaWeight = eta*
      n.output*
      self.gradient
      +alpha*oldDeltaWeight
      self.weights[i] = self.weights[i] + newDeltaWeight
    end
  end
}

--Layer
local Layer = {
  new = function(numNeurons,numInputs,first)
    local self = {}
    for i=1,numNeurons do
      self[i] = Neuron.new(numInputs,first)
    end
    setmetatable(self,{__index = Layer})
    return self
  end,

  run = function(self,input,isInput)
    print(textutils.serialize(input))
    if isInput then
      local output = {[0] = 1}
      for i=1,#input do
        output[i] = input[i]
      end
      return output
    end
    local output = {[0] = 1}
    for i=1,#self do
      print("Running neuron "..tostring(i))
      output[i] = Neuron.run(self[i],input)
    end
    return output
  end
}

--Network
local Network = {
  new = function(topology)
    local self = {topology = {},m_error = 0}
    self.topology[1] = Layer.new(topology[1],1,true)
    for i=2, #topology do
      self.topology[i] = Layer.new(topology[i],topology[i-1])
    end
    setmetatable(self,{__index = Network})
    return self
  end,

  feedForward = function(self,input)
    if #input ~= #self.topology[1] then return end
    print("Running layer 1")
    local output = Layer.run(self.topology[1],input,true)
    for i=2,#self.topology do
      print("Running layer "..tostring(i))
      output = Layer.run(self.topology[i],output,false)
    end
    output[0] = nil
    return output
  end,

  backPropagation = function(self,output)
    local m_error = 0
    for i=1,#self.topology[#self.topology] do
    --  print(type(self.topology[#self.topology][i].output))
      local delta = output[i] - self.topology[#self.topology][i].output
      m_error = m_error + delta * delta
    end
    m_error = m_error/#self.topology[#self.topology]
    m_error = math.sqrt(m_error)
    self.m_error = m_error

    for i=1,#self.topology[#self.topology] do
      Neuron.calculateOutputGradient(self.topology[#self.topology][i],output[i])
    end

    for i=#self.topology-1,2,-1 do
      local currentLayer = self.topology[i]
      local nextLayer = self.topology[i+1]
      for m=1,#currentLayer do
        Neuron.calcHiddenGradient(self.topology[i][m],nextLayer,m)
      end
    end

    for i=#self.topology,2,-1 do
      local currentLayer = self.topology[i]
      local prevlayer = self.topology[i-1]
      for m=1,#currentLayer do
        Neuron.updateInputWeights(self.topology[i][m],prevlayer)
      end
    end

  end,

  getResults = function()

  end
}

net = Network.new({2,4,1})
--[[file = fs.open("OmniOS/nnn","w")
file.write(textutils.serialize(net).."\n"..textutils.serialize(getmetatable(net)))
file.close()]]--
l = 1
while true do
  l = l + 1
  if l%50==0 then sleep(0) end
  if l==500 then break end
  Network.feedForward(net,{1,0})
  Network.backPropagation(net,{1})
  Network.feedForward(net,{1,1})
  Network.backPropagation(net,{0})
  Network.feedForward(net,{0,1})
  Network.backPropagation(net,{1})
  Network.feedForward(net,{0,0})
  Network.backPropagation(net,0)
end
output = Network.feedForward(net,{1,0})
print("Yay "..textutils.serialize(output))
